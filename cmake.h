/* cmake.h.in. Creates cmake.h during a build */

/* Package information */
#define PACKAGE "clog"
#define VERSION "1.3.0"
#define PACKAGE_BUGREPORT "support@taskwarrior.org"
#define PACKAGE_NAME      "clog"
#define PACKAGE_TARNAME   "clog"
#define PACKAGE_VERSION   "1.3.0"
#define PACKAGE_STRING    "clog 1.3.0"

/* git information */
/* #undef HAVE_COMMIT */

/* Compiling platform */
/* #undef LINUX */
#define DARWIN
/* #undef KFREEBSD */
/* #undef FREEBSD */
/* #undef OPENBSD */
/* #undef NETBSD */
/* #undef SOLARIS */
/* #undef GNUHURD */
/* #undef CYGWIN */
/* #undef UNKNOWN */

